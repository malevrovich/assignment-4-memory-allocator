#include "tests.h"

extern inline void run_tests();

int main() {
    run_tests();
    return 0;
}