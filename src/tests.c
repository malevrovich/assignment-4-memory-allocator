#define _DEFAULT_SOURCE

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <string.h>
#include <sys/mman.h>

#define HEAP_SIZE (capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes)

void test_success() {
    heap_init(HEAP_SIZE);
    void *pointer = _malloc(HEAP_SIZE / 8);

    debug_heap(stdout, HEAP_START);
    assert(pointer != NULL);


    for(int i = 0; i < HEAP_SIZE / 8; i++) ((char *) pointer)[i] = '0';
    debug_heap(stdout, HEAP_START);

    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
}

void test_one_free() {
    heap_init(HEAP_SIZE);
    void *addr[7];
    
    debug_heap(stdout, HEAP_START);
    for(int i = 0; i < 7; i++) {
        addr[i] = _malloc(HEAP_SIZE / 8);
        assert(addr[i] != NULL);
    }
    debug_heap(stdout, HEAP_START);

    _free(addr[2]);

    debug_heap(stdout, HEAP_START);
    for(int i = 0; i < 7; i++) {
        if(i != 2) {
            assert(!block_get_header(addr[i])->is_free); 
        }
    }
    assert(block_get_header(addr[2])->is_free);

    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
}

void test_two_free() {
    heap_init(HEAP_SIZE);
    void *addr[7];

    debug_heap(stdout, HEAP_START);
    for(int i = 0; i < 7; i++) {
        addr[i] = _malloc(HEAP_SIZE / 8);
        assert(addr[i] != NULL);
    }
    debug_heap(stdout, HEAP_START);

    _free(addr[2]);
    _free(addr[4]);

    debug_heap(stdout, HEAP_START);
    assert(block_get_header(addr[2])->is_free && block_get_header(addr[4])->is_free);

    _free(addr[3]);

    debug_heap(stdout, HEAP_START);
    
    void *big_floppa_region = _malloc(5 * HEAP_SIZE / 16);

    debug_heap(stdout, HEAP_START);
    assert(big_floppa_region != NULL);
    for(int i = 0; i < 5 * HEAP_SIZE / 16; i++) ((char *) big_floppa_region)[i] = '1';

    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
}

void test_extend() {
    heap_init(HEAP_SIZE);
    
    void *ptr1 = _malloc(HEAP_SIZE);
    void *ptr2 = _malloc(HEAP_SIZE);

    debug_heap(stdout, HEAP_START);
    assert(ptr1 != NULL && ptr2 != NULL);
    struct block_header *start = block_get_header(ptr1);
    
    _free(ptr1);
    _free(ptr2);

    debug_heap(stdout, HEAP_START);

    void *ptr3 = _malloc(2 * HEAP_SIZE);
    debug_heap(stdout, HEAP_START);
    assert(start == block_get_header(ptr3));
    
    munmap(HEAP_START, 2 * size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
}

void test_unable_to_extend() {
    heap_init(HEAP_SIZE);

    void *ptr1 = _malloc(HEAP_SIZE);
    void *ptr2 = mmap(ptr1 + HEAP_SIZE, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);
    
    debug_heap(stdout, HEAP_START);
    assert(ptr1 != NULL && ptr2 == ptr1 + HEAP_SIZE);
    struct block_header *start = block_get_header(ptr1);
    
    void *ptr3 = _malloc(HEAP_SIZE);
    debug_heap(stdout, HEAP_START);

    assert(ptr3 != NULL);
    assert(start != block_get_header(ptr3));

    _free(ptr1);
    _free(ptr3);
    debug_heap(stdout, HEAP_START);

    void *ptr4 = _malloc(2 * HEAP_SIZE);

    debug_heap(stdout, HEAP_START);

    assert(start != block_get_header(ptr4));
    
    munmap(HEAP_START, size_from_capacity((block_capacity) { HEAP_SIZE }).bytes);
    munmap(HEAP_START, HEAP_SIZE);
    munmap(block_get_header(ptr4), size_from_capacity((block_capacity) {HEAP_SIZE}).bytes);
}